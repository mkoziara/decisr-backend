# README #

Backend dla aplikacji decisr. Aby wybudować projekt wystarczy prosta komenda 
```
#!java

gradlew build
```

która utworzy tzw. executable jara czyli plik który jesteśmy w stanie uruchomić np. jako service linuxowy.

Do autoryzacji zostały wykorzystane tokeny JWT, które walidowane oraz zarządzane są przez firebase, googlowy projekt oferujący między innymi zarządzanie użytkownikami. Storage plików również znajduje się w googlowej chmurze.