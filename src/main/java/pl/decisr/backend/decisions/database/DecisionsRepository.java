package pl.decisr.backend.decisions.database;

import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.DecisionState;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DecisionsRepository {

    private final MongoDecisionsRepository mongoDecisionsRepository;
    private final MongoTemplate mongoTemplate;

    public Decision save(Decision decision) {
        return mongoDecisionsRepository.save(decision);
    }

    public Decision findById(String id) {
        return mongoDecisionsRepository.findById(id);
    }

    public List<Decision> findByOwnerId(String ownerId) {
        return mongoDecisionsRepository.findByOwnerId(ownerId);
    }

    public Decision addAnswer(Decision decision, DecisionAnswer decisionAnswer) {
        decision.addAnswer(decisionAnswer);
        return mongoDecisionsRepository.save(decision);
    }

    public List<Decision> findUnansweredNotMineDecisions(String ownerId) {
        Criteria criteria = Criteria.where("ownerId").ne(ownerId)
                .and("answers.answeredBy").ne(ownerId)
                .and(Decision.DECISION_STATE_FIELD).ne(DecisionState.FINISHED);
        Query query = Query.query(criteria);

        return mongoTemplate.find(query, Decision.class);
    }
}
