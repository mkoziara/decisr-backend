package pl.decisr.backend.decisions.database;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionState;

import java.time.LocalDateTime;

@Component
class TtlDecisionsFinisher {

    private final MongoTemplate mongoTemplate;

    TtlDecisionsFinisher(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Scheduled(cron = "1 */10 * * * *")
    void finishDecisions() {
        LocalDateTime currentDateTime = LocalDateTime.now();

        Criteria criteria = Criteria
                .where(Decision.DECISION_STATE_FIELD).is(DecisionState.TIME_TO_LIVE.toString())
                .and(Decision.FINISH_TIME_FIELD).lte(currentDateTime);
        Query.query(criteria);

        Update update = Update.update(Decision.DECISION_STATE_FIELD, DecisionState.FINISHED);
        mongoTemplate.updateMulti(Query.query(criteria), update, Decision.class);
    }
}
