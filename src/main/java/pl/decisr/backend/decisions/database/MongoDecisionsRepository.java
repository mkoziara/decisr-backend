package pl.decisr.backend.decisions.database;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionState;

import java.util.List;

interface MongoDecisionsRepository extends MongoRepository<Decision, String> {

    Decision findById(String id);

    List<Decision> findByOwnerId(String ownerId);

    List<Decision> findByDecisionStateEquals(DecisionState state);
}
