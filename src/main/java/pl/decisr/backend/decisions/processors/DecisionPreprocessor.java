package pl.decisr.backend.decisions.processors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DecisionPreprocessor {

    private final List<DecisionProcessor> decisionProcessors;

    public Decision processDecision(Decision decision) {
        return decisionProcessors.stream()
                .filter(p -> p.canProcess(decision))
                .findFirst()
                .map(p -> p.process(decision))
                .map(d -> copyBasicData(d, decision))
                .orElseThrow(RuntimeException::new);
    }

    private Decision copyBasicData(Decision newDecision, Decision oldDecision) {
        newDecision.setDecisionState(oldDecision.getDecisionState());
        newDecision.setFinishTime(oldDecision.getFinishTime());

        return newDecision;
    }
}