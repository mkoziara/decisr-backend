package pl.decisr.backend.decisions.processors;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;

@Component
@Order
class DefaultDecisionProcessor implements DecisionProcessor {

    @Override
    public boolean canProcess(Decision decision) {
        return true;
    }

    @Override
    public Decision process(Decision decision) {
        return decision;
    }
}
