package pl.decisr.backend.decisions.processors;

import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.LinkDecisionContentService;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.link.LinkDecision;
import pl.decisr.backend.decisions.model.simple.SimpleChoiceDecision;
import pl.decisr.backend.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Order(2)
@RequiredArgsConstructor
class LinkDecisionProcessor implements DecisionProcessor {

    private final FirebaseStorage storage;
    private final DocumentParserService elementsService;
    private final LinkDecisionContentService linkDecisionContentService;

    @Override
    public boolean canProcess(Decision decision) {
        return decision instanceof LinkDecision;
    }

    @Override
    public Decision process(Decision decision) {
        LinkDecision linkDecision = (LinkDecision) decision;

        ArrayList<Document> documents = new ArrayList<>();
        documents.add(elementsService.getDocument(linkDecision.getFirstLink()));
        documents.add(elementsService.getDocument(linkDecision.getSecondLink()));

        String fullTitle = elementsService.createVersusTitle(documents.get(0), documents.get(1));

        String ownerId = decision.getOwnerId();
        List<String> imagePaths = documents.stream()
                .map(elementsService::getImageURL)
                .map(o -> o.isPresent() ? storage.storeFileFromUrl(o.get(), ownerId) : null)
                .collect(Collectors.toList());

        SimpleChoiceDecision simpleChoiceDecision =
                new SimpleChoiceDecision(fullTitle, imagePaths.get(0), imagePaths.get(1));

        return new LinkDecision(simpleChoiceDecision,
                linkDecision.getFirstLink(),
                linkDecision.getSecondLink(),
                linkDecisionContentService);
    }
}