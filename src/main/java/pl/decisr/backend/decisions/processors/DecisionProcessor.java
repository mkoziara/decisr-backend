package pl.decisr.backend.decisions.processors;

import pl.decisr.backend.decisions.model.Decision;

public interface DecisionProcessor {

    boolean canProcess(Decision decision);

    Decision process(Decision decision);
}
