package pl.decisr.backend.decisions.processors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
public class DocumentParserService {

    public Document getDocument(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String createVersusTitle(Document firstDocument, Document secondDocument) {
        String firstTitle = getTitle(firstDocument);
        String secondTitle = getTitle(secondDocument);
        return String.format("%s vs %s", firstTitle, secondTitle);
    }

    public Optional<String> getImageURL(Document document) {
        return getOgProperty(document, "image");
    }

    private String getTitle(Document document) {
        Optional<String> title = getOgProperty(document, "title");
        if (title.isPresent()) {
            return title.get();
        } else {
            return getDefaultDocumentTitle(document);
        }
    }

    private String getDefaultDocumentTitle(Document document) {
        Element titleElement = document.getElementsByTag("title").first();
        return Optional
                .ofNullable(titleElement)
                .map(Element::ownText)
                .orElse("Option");
    }

    private Optional<String> getOgProperty(Document document, String propertyName) {
        Element titleElement = document
                .getElementsByAttributeValue("property", "og:" + propertyName)
                .first();
        return Optional
                .ofNullable(titleElement)
                .map(e -> e.attr("content"));
    }
}
