package pl.decisr.backend.decisions;

import org.springframework.stereotype.Service;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionType;
import pl.decisr.backend.decisions.model.link.LinkDecision;

@Service
public class LinkDecisionContentService {

    public Decision getDecisionContent(Decision decision) {
        if (decision.getType().equals(DecisionType.LINK)) {
            return ((LinkDecision) decision).getContent();
        }
        return decision;
    }

    public Decision fillInContentInfo(Decision source, Decision destination) {
        destination.setId(source.getId());
        destination.setOwnerId(source.getOwnerId());
        destination.setCreateTime(source.getCreateTime());
        destination.setDecisionState(source.getDecisionState());
        destination.setFinishTime(source.getFinishTime());
        return destination;
    }
}
