package pl.decisr.backend.decisions.validators.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Decision not found!")
public class DecisionNotFoundException extends RuntimeException {
}
