package pl.decisr.backend.decisions.validators;

import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.validators.exceptions.AlreadyAnsweredException;

import java.security.Principal;

@Component
public class AlreadyAnsweredValidator {

    public void validateUserDidNotAlreadyAnswered(Decision decision, Principal principal) {

        boolean result = decision
                .getAnswers()
                .stream()
                .anyMatch(d -> d.getAnsweredBy().equals(principal.getName()));

        if (result) {
            throw new AlreadyAnsweredException();
        }
    }
}
