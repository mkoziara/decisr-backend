package pl.decisr.backend.decisions.validators;

import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionState;
import pl.decisr.backend.decisions.validators.exceptions.DecisionAlreadyFinishedException;

@Component
public class NotFinishedDecisionValidator {

    public void validateDecisionIsNotFinished(Decision decision) {

        if (decision.getDecisionState().equals(DecisionState.FINISHED)) {
            throw new DecisionAlreadyFinishedException();
        }
    }
}
