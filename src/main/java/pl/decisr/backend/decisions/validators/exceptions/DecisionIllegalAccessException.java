package pl.decisr.backend.decisions.validators.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Action can not be performed on someone else's decision!")
public class DecisionIllegalAccessException extends RuntimeException {
}
