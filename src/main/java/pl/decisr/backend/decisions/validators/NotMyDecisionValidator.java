package pl.decisr.backend.decisions.validators;

import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;

import java.security.Principal;

@Component
public class NotMyDecisionValidator {

    public void validateDecisionIsNotMine(Decision decision, Principal principal) {

        if (decision.getOwnerId().equals(principal.getName())) {
            throw new IllegalArgumentException("You can't answer to your own");
        }
    }
}
