package pl.decisr.backend.decisions.validators.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "User already answered!")
public class AlreadyAnsweredException extends RuntimeException {
}
