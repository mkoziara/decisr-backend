package pl.decisr.backend.decisions.validators;

import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.validators.exceptions.DecisionIllegalAccessException;

import java.security.Principal;

@Component
public class DecisionOwnerValidator {

    public void validateDecisionIsOwnedByUser(Decision decision, Principal user) {
        if (!isDecisionOwnedByUser(decision, user)) {
            throw new DecisionIllegalAccessException();
        }
    }

    private boolean isDecisionOwnedByUser(Decision decision, Principal user) {
        return decision.getOwnerId().equals(user.getName());
    }

}
