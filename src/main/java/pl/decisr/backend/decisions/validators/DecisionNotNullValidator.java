package pl.decisr.backend.decisions.validators;

import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.validators.exceptions.DecisionNotFoundException;

@Component
public class DecisionNotNullValidator {

    public void validateDecisionIsNotNull(Decision decision) {
        if(decision == null) {
            throw new DecisionNotFoundException();
        }
    }

}
