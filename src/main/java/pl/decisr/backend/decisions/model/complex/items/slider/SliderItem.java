package pl.decisr.backend.decisions.model.complex.items.slider;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;
import pl.decisr.backend.decisions.model.complex.items.DecisionItem;
import pl.decisr.backend.decisions.model.complex.items.DecisionItemType;

import javax.validation.constraints.NotNull;

@RequiredArgsConstructor
@Getter
public class SliderItem extends DecisionItem {

    @NotNull
    private final Double bottomValue;

    @NotNull
    private final Double topValue;

    @Override
    public DecisionItemType getType() {
        return DecisionItemType.SLIDER;
    }

    @Override
    public boolean canAcceptAnswer(ItemAnswer answer) {
        Double value = Double.valueOf(answer.getAnswer().toString());
        return value >= bottomValue &&
                value <= topValue;
    }

}
