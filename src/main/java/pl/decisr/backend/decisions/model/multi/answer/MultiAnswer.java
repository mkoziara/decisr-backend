package pl.decisr.backend.decisions.model.multi.answer;

import lombok.Value;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.DecisionType;

import java.util.List;

@Value
public class MultiAnswer extends DecisionAnswer {

    private List<MultiSubdecisionAnswer> answers;

    @Override
    public DecisionType getDecisionAnswerType() {
        return DecisionType.MULTI;
    }
}
