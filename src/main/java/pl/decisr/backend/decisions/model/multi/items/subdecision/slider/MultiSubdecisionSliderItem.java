package pl.decisr.backend.decisions.model.multi.items.subdecision.slider;


import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionItemAnswer;
import pl.decisr.backend.decisions.model.multi.items.subdecision.MultiSubdecisionItem;
import pl.decisr.backend.decisions.model.multi.items.subdecision.MultiSubdecisionItemType;

public class MultiSubdecisionSliderItem extends MultiSubdecisionItem {

    @Override
    public MultiSubdecisionItemType getType() {
        return MultiSubdecisionItemType.MULTI_SUBSLIDER;
    }

    @Override
    public boolean canAcceptAnswer(MultiSubdecisionItemAnswer itemAnswer) {
        Double answer = itemAnswer.getAnswer();
        return !(answer < 0 || answer > 100);
    }
}
