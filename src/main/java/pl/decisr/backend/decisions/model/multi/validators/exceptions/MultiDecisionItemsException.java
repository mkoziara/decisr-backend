package pl.decisr.backend.decisions.model.multi.validators.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Subitems must correspond to main decision items")
public class MultiDecisionItemsException extends RuntimeException {
}
