package pl.decisr.backend.decisions.model.complex.items;

public enum DecisionItemType {
    EITHER_OR, SLIDER
}
