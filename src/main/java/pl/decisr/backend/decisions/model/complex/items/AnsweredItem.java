package pl.decisr.backend.decisions.model.complex.items;

import lombok.Value;
import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;

import java.util.List;

@Value
public class AnsweredItem {

    private final DecisionItem item;
    private final List<ItemAnswer> answers;

    public DecisionItemType getItemType() {
        return item.getType();
    }
}
