package pl.decisr.backend.decisions.model.multi;


import lombok.Getter;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionType;
import pl.decisr.backend.decisions.model.complex.exceptions.ItemNotFoundException;
import pl.decisr.backend.decisions.model.multi.items.decision.MultiDecisionItem;

import java.util.ArrayList;
import java.util.List;


public class MultiDecision extends Decision {

    @Getter
    private List<MultiDecisionItem> items = new ArrayList<>();

    @Getter
    private List<MultiSubdecision> subdecisions = new ArrayList<>();

    @Override
    public DecisionType getType() {
        return DecisionType.MULTI;
    }

    public MultiSubdecision findSubdecisionById(String id) {
        return subdecisions.stream()
                .filter(i -> i.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new ItemNotFoundException(id));
    }
}
