package pl.decisr.backend.decisions.model.complex.items.slider;


import lombok.RequiredArgsConstructor;
import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;

@RequiredArgsConstructor
public class SliderItemAnswer implements ItemAnswer<Double> {

    private final String itemId;
    private final Double answer;

    @Override
    public String getItemId() {
        return itemId;
    }

    @Override
    public Double getAnswer() {
        return answer;
    }
}
