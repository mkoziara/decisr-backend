package pl.decisr.backend.decisions.model.complex.answer;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.DecisionType;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Getter
public class ComplexAnswer extends DecisionAnswer {

    @NotEmpty
    private List<ItemAnswer> itemsAnswers = new ArrayList<>();

    @Override
    public DecisionType getDecisionAnswerType() {
        return DecisionType.COMPLEX;
    }
}
