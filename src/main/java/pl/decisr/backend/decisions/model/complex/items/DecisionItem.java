package pl.decisr.backend.decisions.model.complex.items;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;
import pl.decisr.backend.decisions.model.complex.items.eitheror.EitherOrItem;
import pl.decisr.backend.decisions.model.complex.items.slider.SliderItem;

@Document
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EitherOrItem.class, name = "EITHER_OR"),
        @JsonSubTypes.Type(value = SliderItem.class, name = "SLIDER")
})
@RequiredArgsConstructor
@Getter
public abstract class DecisionItem {

    @Id
    private String id = new ObjectId().toHexString();

    @Setter
    private String description;

    public abstract DecisionItemType getType();

    public abstract boolean canAcceptAnswer(ItemAnswer answer);
}
