package pl.decisr.backend.decisions.model.multi.answer;

import lombok.Getter;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.complex.exceptions.ItemNotFoundException;

import java.util.List;


@Document
@Getter
public class MultiSubdecisionAnswer {

    private String decisionId;
    private List<MultiSubdecisionItemAnswer> itemAnswers;

    public MultiSubdecisionItemAnswer findItemAnswerById(String id) {
        return itemAnswers.stream()
                .filter(i -> i.getItemId().equals(id))
                .findFirst()
                .orElseThrow(() -> new ItemNotFoundException(id));
    }

}
