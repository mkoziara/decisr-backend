package pl.decisr.backend.decisions.model.multi.items.decision;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.multi.items.decision.slider.MultiSliderItem;

@Document
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MultiSliderItem.class, name = "MULTI_SLIDER")
})
@RequiredArgsConstructor
@Getter
public abstract class MultiDecisionItem {

    @Id
    private String id = new ObjectId().toHexString();

    private String name;

    private Double factor;

    public abstract MultiDecisionItemType getType();
}
