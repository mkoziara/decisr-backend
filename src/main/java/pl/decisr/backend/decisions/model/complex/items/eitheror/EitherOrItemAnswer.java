package pl.decisr.backend.decisions.model.complex.items.eitheror;


import lombok.RequiredArgsConstructor;
import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;

@RequiredArgsConstructor
public class EitherOrItemAnswer implements ItemAnswer<String> {

    private final String itemId;
    private final String answer;

    @Override
    public String getItemId() {
        return itemId;
    }

    @Override
    public String getAnswer() {
        return answer;
    }
}
