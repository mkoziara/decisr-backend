package pl.decisr.backend.decisions.model.multi;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.multi.items.subdecision.MultiSubdecisionItem;

import java.util.ArrayList;
import java.util.List;

@Document
@RequiredArgsConstructor
@Getter
public class MultiSubdecision {

    @Id
    private String id = new ObjectId().toHexString();

    private final String title;
    private final String image;

    private List<MultiSubdecisionItem> items = new ArrayList<>();
}
