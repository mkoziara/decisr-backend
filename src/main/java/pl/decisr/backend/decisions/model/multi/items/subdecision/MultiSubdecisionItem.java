package pl.decisr.backend.decisions.model.multi.items.subdecision;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionItemAnswer;
import pl.decisr.backend.decisions.model.multi.items.subdecision.slider.MultiSubdecisionSliderItem;

@Document
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MultiSubdecisionSliderItem.class, name = "MULTI_SUBSLIDER")
})
@RequiredArgsConstructor
@Getter
public abstract class MultiSubdecisionItem {

    private String itemId;

    private String value;

    public abstract MultiSubdecisionItemType getType();

    public abstract boolean canAcceptAnswer(MultiSubdecisionItemAnswer answer);
}
