package pl.decisr.backend.decisions.model.complex.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "IDs of items and answers must match")
public class IdsNotMatchingException extends RuntimeException {
}
