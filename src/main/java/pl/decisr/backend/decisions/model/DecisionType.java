package pl.decisr.backend.decisions.model;

public enum DecisionType {
    SIMPLE_CHOICE, LINK, COMPLEX, MULTI
}
