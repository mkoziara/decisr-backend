package pl.decisr.backend.decisions.model;

public enum DecisionState  {
    TIME_TO_LIVE, INFINITE_TIME, FINISHED
}
