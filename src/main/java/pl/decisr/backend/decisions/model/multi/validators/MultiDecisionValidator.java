package pl.decisr.backend.decisions.model.multi.validators;


import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.multi.MultiDecision;
import pl.decisr.backend.decisions.model.multi.MultiSubdecision;
import pl.decisr.backend.decisions.model.multi.items.decision.MultiDecisionItem;
import pl.decisr.backend.decisions.model.multi.items.subdecision.MultiSubdecisionItem;
import pl.decisr.backend.decisions.model.multi.validators.exceptions.InvalidFactorException;
import pl.decisr.backend.decisions.model.multi.validators.exceptions.MultiDecisionItemsException;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MultiDecisionValidator {

    public boolean canValidate(Decision decision) {
        return decision instanceof MultiDecision;
    }

    public void validate(Decision decision) {
        MultiDecision multiDecision = (MultiDecision) decision;

        List<MultiDecisionItem> items = multiDecision.getItems();
        List<MultiSubdecision> subdecisions = multiDecision.getSubdecisions();
        validateItems(items, subdecisions);
        validateFactors(items);
    }

    private void validateItems(List<MultiDecisionItem> items,
                               List<MultiSubdecision> subdecisions) {
        List<String> itemsIds = items.stream()
                .map(MultiDecisionItem::getId)
                .collect(Collectors.toList());
        for (MultiSubdecision subdecision : subdecisions) {
            List<String> subdecisionItemsIds = subdecision.getItems().stream()
                    .map(MultiSubdecisionItem::getItemId)
                    .collect(Collectors.toList());
            if (!subdecisionItemsIds.containsAll(itemsIds)) {
                throw new MultiDecisionItemsException();
            }
            if (subdecisionItemsIds.size() != itemsIds.size()) {
                throw new MultiDecisionItemsException();
            }
        }
    }

    private void validateFactors(List<MultiDecisionItem> items) {
        items.stream()
                .filter(item -> item.getFactor() < 0 || item.getFactor() > 100)
                .findAny()
                .ifPresent(item -> {
                    throw new InvalidFactorException(item.getId());
                });
    }
}
