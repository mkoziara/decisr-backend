package pl.decisr.backend.decisions.model.simple;

import lombok.Value;
import pl.decisr.backend.decisions.model.AnswerSummary;

@Value
public class SimpleChoiceAnswerSummary implements AnswerSummary {

    private final Long leftAnswers;
    private final Long rightAnswers;
}
