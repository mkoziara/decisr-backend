package pl.decisr.backend.decisions.model.multi.answer;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import pl.decisr.backend.decisions.model.multi.items.subdecision.slider.MultiSubdecisionSliderItemAnswer;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MultiSubdecisionSliderItemAnswer.class, name = "SLIDER_ITEM_ANSWER")
})
public interface MultiSubdecisionItemAnswer {

    String getItemId();

    Double getAnswer();

}
