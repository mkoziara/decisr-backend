package pl.decisr.backend.decisions.model.multi.items.subdecision.slider;


import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionItemAnswer;

public class MultiSubdecisionSliderItemAnswer implements MultiSubdecisionItemAnswer {

    private String itemId;
    private Double answer;

    @Override
    public String getItemId() {
        return itemId;
    }

    @Override
    public Double getAnswer() {
        return answer;
    }
}
