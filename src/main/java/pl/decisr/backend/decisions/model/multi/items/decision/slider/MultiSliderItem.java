package pl.decisr.backend.decisions.model.multi.items.decision.slider;


import pl.decisr.backend.decisions.model.multi.items.decision.MultiDecisionItem;
import pl.decisr.backend.decisions.model.multi.items.decision.MultiDecisionItemType;

public class MultiSliderItem extends MultiDecisionItem {

    @Override
    public MultiDecisionItemType getType() {
        return MultiDecisionItemType.MULTI_SLIDER;
    }
}
