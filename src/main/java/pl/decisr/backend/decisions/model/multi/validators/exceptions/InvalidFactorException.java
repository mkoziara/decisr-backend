package pl.decisr.backend.decisions.model.multi.validators.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidFactorException extends RuntimeException {
    public InvalidFactorException(String itemId) {
        super("Invalid factor for item with id: " + itemId +
                ". Item factor must be a value between 0 and 100.");
    }
}
