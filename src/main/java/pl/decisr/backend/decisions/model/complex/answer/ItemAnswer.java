package pl.decisr.backend.decisions.model.complex.answer;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import pl.decisr.backend.decisions.model.complex.items.eitheror.EitherOrItemAnswer;
import pl.decisr.backend.decisions.model.complex.items.slider.SliderItemAnswer;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EitherOrItemAnswer.class, name = "EITHER_OR_ITEM_ANSWER"),
        @JsonSubTypes.Type(value = SliderItemAnswer.class, name = "SLIDER_ITEM_ANSWER")
})
public interface ItemAnswer<T> {

    String getItemId();

    T getAnswer();
}
