package pl.decisr.backend.decisions.model.multi.validators;

import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.complex.exceptions.AnswerConstraintViolationException;
import pl.decisr.backend.decisions.model.multi.MultiDecision;
import pl.decisr.backend.decisions.model.multi.MultiSubdecision;
import pl.decisr.backend.decisions.model.multi.answer.MultiAnswer;
import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionAnswer;
import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionItemAnswer;
import pl.decisr.backend.decisions.model.multi.items.decision.MultiDecisionItem;
import pl.decisr.backend.decisions.model.multi.validators.exceptions.MultiDecisionItemsException;
import pl.decisr.backend.decisions.model.multi.validators.exceptions.MultiSubdecisionsException;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MultiAnswerValidator {
    public boolean canValidate(Decision decision, DecisionAnswer answer) {
        return decision instanceof MultiDecision &&
                answer instanceof MultiAnswer;
    }

    public void validate(Decision decision, DecisionAnswer decisionAnswer) {
        MultiDecision multiDecision = (MultiDecision) decision;
        MultiAnswer multiAnswer = (MultiAnswer) decisionAnswer;

        List<MultiSubdecisionAnswer> answers = multiAnswer.getAnswers();

        List<MultiSubdecision> subdecisions = multiDecision.getSubdecisions();
        validateSubdecisions(subdecisions, answers);

        List<MultiDecisionItem> items = multiDecision.getItems();
        validateItems(items, answers);

        validateAnswersAreAcceptable(multiDecision, answers);
    }


    private void validateSubdecisions(List<MultiSubdecision> subdecisions,
                                      List<MultiSubdecisionAnswer> answers) {
        List<String> subdecisionsIds = subdecisions.stream()
                .map(MultiSubdecision::getId)
                .collect(Collectors.toList());

        List<String> answersIds = answers.stream()
                .map(MultiSubdecisionAnswer::getDecisionId)
                .collect(Collectors.toList());
        if (!answersIds.containsAll(subdecisionsIds)) {
            throw new MultiSubdecisionsException();
        }
        if (answersIds.size() != subdecisionsIds.size()) {
            throw new MultiSubdecisionsException();
        }
    }

    private void validateItems(List<MultiDecisionItem> items,
                               List<MultiSubdecisionAnswer> answers) {
        List<String> itemsIds = items.stream()
                .map(MultiDecisionItem::getId)
                .collect(Collectors.toList());
        for (MultiSubdecisionAnswer answer : answers) {
            List<String> itemAnswersIds = answer.getItemAnswers().stream()
                    .map(MultiSubdecisionItemAnswer::getItemId)
                    .collect(Collectors.toList());
            if (!itemAnswersIds.containsAll(itemsIds)) {
                throw new MultiDecisionItemsException();
            }
            if (itemAnswersIds.size() != itemsIds.size()) {
                throw new MultiDecisionItemsException();
            }
        }
    }

    private void validateAnswersAreAcceptable(MultiDecision decision,
                                              List<MultiSubdecisionAnswer> answers) {
        for (MultiSubdecisionAnswer answer : answers) {
            MultiSubdecision subdecision = decision.findSubdecisionById(answer.getDecisionId());
            subdecision.getItems().stream()
                    .map(subdecisionItem ->
                            Pair.of(subdecisionItem,
                                    answer.findItemAnswerById(subdecisionItem.getItemId())))
                    .filter(p -> !p.getFirst().canAcceptAnswer(p.getSecond()))
                    .findAny()
                    .map(p -> p.getSecond().getItemId())
                    .ifPresent(id -> {
                        throw new AnswerConstraintViolationException(id);
                    });
        }
    }
}
