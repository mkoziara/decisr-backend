package pl.decisr.backend.decisions.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.complex.ComplexDecision;
import pl.decisr.backend.decisions.model.link.LinkDecision;
import pl.decisr.backend.decisions.model.multi.MultiDecision;
import pl.decisr.backend.decisions.model.simple.SimpleChoiceDecision;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SimpleChoiceDecision.class, name = "SIMPLE_CHOICE"),
        @JsonSubTypes.Type(value = LinkDecision.class, name = "LINK"),
        @JsonSubTypes.Type(value = ComplexDecision.class, name = "COMPLEX"),
        @JsonSubTypes.Type(value = MultiDecision.class, name = "MULTI")
})
@Getter
public abstract class Decision {

    public static final String CREATE_TIME_FIELD = "createTime";
    public static final String DECISION_STATE_FIELD = "decisionState";
    public static final String FINISH_TIME_FIELD = "finishTime";

    @Id
    @Setter
    private String id;

    @CreatedDate
    @Setter
    private LocalDateTime createTime;

    @Setter
    private LocalDateTime finishTime;

    @Setter
    private DecisionState decisionState;

    @JsonIgnore
    @Transient
    @Getter(onMethod = @__(@JsonProperty))
    @Setter(onMethod = @__(@JsonIgnore))
    private AnswerSummary answerSummary;

    @Setter
    private String ownerId;

    private List<DecisionAnswer> answers = new ArrayList<>();

    public void addAnswer(DecisionAnswer decisionAnswer) {
        answers.add(decisionAnswer);
    }

    public abstract DecisionType getType();
}
