package pl.decisr.backend.decisions.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import pl.decisr.backend.decisions.model.complex.answer.ComplexAnswer;
import pl.decisr.backend.decisions.model.multi.answer.MultiAnswer;
import pl.decisr.backend.decisions.model.simple.SimpleChoiceAnswer;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "decisionAnswerType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SimpleChoiceAnswer.class, name = "SIMPLE_CHOICE"),
        @JsonSubTypes.Type(value = ComplexAnswer.class, name = "COMPLEX"),
        @JsonSubTypes.Type(value = MultiAnswer.class, name = "MULTI")
})
@Data
public abstract class DecisionAnswer {

    private String answeredBy;

    public abstract DecisionType getDecisionAnswerType();
}
