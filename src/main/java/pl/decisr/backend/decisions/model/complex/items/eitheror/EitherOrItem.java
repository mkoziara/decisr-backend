package pl.decisr.backend.decisions.model.complex.items.eitheror;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;
import pl.decisr.backend.decisions.model.complex.items.DecisionItem;
import pl.decisr.backend.decisions.model.complex.items.DecisionItemType;

@Document
@RequiredArgsConstructor
@Getter
public class EitherOrItem extends DecisionItem {

    private final String leftValue;
    private final String rightValue;

    @Override
    public DecisionItemType getType() {
        return DecisionItemType.EITHER_OR;
    }

    @Override
    public boolean canAcceptAnswer(ItemAnswer answer) {
        String value = (String) answer.getAnswer();
        return leftValue.equals(value) || rightValue.equals(value);
    }
}
