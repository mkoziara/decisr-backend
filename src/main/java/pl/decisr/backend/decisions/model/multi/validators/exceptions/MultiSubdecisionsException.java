package pl.decisr.backend.decisions.model.multi.validators.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "MultiAnswer must contain answers to all subdecisions")
public class MultiSubdecisionsException extends RuntimeException {
}
