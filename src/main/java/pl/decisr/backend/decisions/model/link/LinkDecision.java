package pl.decisr.backend.decisions.model.link;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.LinkDecisionContentService;
import pl.decisr.backend.decisions.model.*;

import java.time.LocalDateTime;
import java.util.List;

@Document
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator))
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LinkDecision<T extends Decision> extends Decision {

    @JsonIgnore
    private final LinkDecisionContentService linkDecisionContentService;

    @Setter
    private T content;

    @Getter
    private final String firstLink;

    @Getter
    private final String secondLink;

    @PersistenceConstructor
    public LinkDecision(T content,
                        String firstLink,
                        String secondLink,
                        LinkDecisionContentService linkDecisionContentService) {
        this.content = content;
        this.firstLink = firstLink;
        this.secondLink = secondLink;
        this.linkDecisionContentService = linkDecisionContentService;
    }

    public Decision getContent() {
        linkDecisionContentService.fillInContentInfo(this, content);
        return content;
    }

    @Override
    public void addAnswer(DecisionAnswer decisionAnswer) {
        content.addAnswer(decisionAnswer);
    }

    @Override
    public DecisionType getType() {
        return DecisionType.LINK;
    }

    @JsonIgnore
    @Override
    public AnswerSummary getAnswerSummary() {
        return content.getAnswerSummary();
    }

    @Override
    public void setAnswerSummary(AnswerSummary answerSummary) {
        content.setAnswerSummary(answerSummary);
    }

    @JsonIgnore
    @Override
    public String getId() {
        return super.getId();
    }

    @JsonIgnore
    @Override
    public LocalDateTime getCreateTime() {
        return super.getCreateTime();
    }

    @JsonIgnore
    @Override
    public LocalDateTime getFinishTime() {
        return super.getFinishTime();
    }

    @JsonIgnore
    @Override
    public DecisionState getDecisionState() {
        return super.getDecisionState();
    }

    @JsonIgnore
    @Override
    public String getOwnerId() {
        return super.getOwnerId();
    }

    @JsonIgnore
    @Override
    public List<DecisionAnswer> getAnswers() {
        return content.getAnswers();
    }

    @JsonProperty
    @Override
    public void setDecisionState(DecisionState state) {
        super.setDecisionState(state);
    }

    @JsonProperty
    @Override
    public void setId(String id) {
        super.setId(id);
    }

    @JsonProperty
    @Override
    public void setCreateTime(LocalDateTime createTime) {
        super.setCreateTime(createTime);
    }

    @JsonProperty
    @Override
    public void setFinishTime(LocalDateTime finishTime) {
        super.setFinishTime(finishTime);
    }

    @JsonProperty
    @Override
    public void setOwnerId(String ownerId) {
        super.setOwnerId(ownerId);
    }
}
