package pl.decisr.backend.decisions.model.complex.answer;

import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.LinkDecisionContentService;
import pl.decisr.backend.decisions.database.DecisionsRepository;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.DecisionType;
import pl.decisr.backend.decisions.model.complex.ComplexDecision;
import pl.decisr.backend.decisions.model.complex.exceptions.AnswerConstraintViolationException;
import pl.decisr.backend.decisions.model.complex.exceptions.IdsNotMatchingException;
import pl.decisr.backend.decisions.model.complex.items.DecisionItem;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ComplexAnswerValidator {

    private final DecisionsRepository decisionsRepository;
    private final LinkDecisionContentService linkDecisionContentService;

    public boolean canValidate(Decision decision, DecisionAnswer answer) {
        decision = linkDecisionContentService.getDecisionContent(decision);

        return decision.getType().equals(DecisionType.COMPLEX) &&
                answer.getDecisionAnswerType().equals(DecisionType.COMPLEX);
    }

    public void validate(Decision decision, DecisionAnswer decisionAnswer) {
        decision = linkDecisionContentService.getDecisionContent(decision);
        ComplexAnswer answer = (ComplexAnswer) decisionAnswer;
        ComplexDecision complexDecision = (ComplexDecision) decision;

        List<DecisionItem> decisionItems = complexDecision.getItems();
        List<ItemAnswer> itemsAnswers = answer.getItemsAnswers();

        validateIdsMatch(decisionItems, itemsAnswers);
        validateAnswersAreAcceptable(complexDecision, itemsAnswers);
    }

    private void validateIdsMatch(List<DecisionItem> decisionItems, List<ItemAnswer> itemsAnswers) {
        if (!containsAnswersToAllItems(decisionItems, itemsAnswers)) {
            throw new IdsNotMatchingException();
        }
    }

    private void validateAnswersAreAcceptable(ComplexDecision complexDecision, List<ItemAnswer> itemsAnswers) {
        itemsAnswers.stream()
                .map(itemAnswer -> Pair.of(itemAnswer, complexDecision.findItemById(itemAnswer.getItemId())))
                .filter(p -> !p.getSecond().canAcceptAnswer(p.getFirst()))
                .findAny()
                .map(p -> p.getFirst().getItemId())
                .ifPresent(id -> {
                    throw new AnswerConstraintViolationException(id);
                });
    }

    private boolean containsAnswersToAllItems(List<DecisionItem> decisionItems, List<ItemAnswer> itemsAnswers) {
        List<String> decisionItemsIds = decisionItems.stream()
                .map(DecisionItem::getId)
                .collect(Collectors.toList());

        List<String> itemsAnswersIds = itemsAnswers.stream()
                .map(ItemAnswer::getItemId)
                .collect(Collectors.toList());

        return itemsAnswersIds.containsAll(decisionItemsIds);
    }
}
