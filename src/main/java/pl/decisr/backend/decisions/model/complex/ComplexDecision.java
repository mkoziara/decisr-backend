package pl.decisr.backend.decisions.model.complex;

import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionType;
import pl.decisr.backend.decisions.model.complex.exceptions.ItemNotFoundException;
import pl.decisr.backend.decisions.model.complex.items.DecisionItem;

import java.util.ArrayList;
import java.util.List;

@Document
public class ComplexDecision extends Decision {

    private List<DecisionItem> items = new ArrayList<>();

    @Override
    public DecisionType getType() {
        return DecisionType.COMPLEX;
    }

    public List<DecisionItem> getItems() {
        return items;
    }

    public DecisionItem findItemById(String id) {
        return items.stream()
                .filter(i -> i.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new ItemNotFoundException(id));
    }

    public void addItem(DecisionItem decisionItem) {
        items.add(decisionItem);
    }
}
