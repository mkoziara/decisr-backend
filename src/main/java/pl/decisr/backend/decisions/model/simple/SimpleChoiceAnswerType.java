package pl.decisr.backend.decisions.model.simple;

public enum SimpleChoiceAnswerType {
    LEFT, RIGHT
}
