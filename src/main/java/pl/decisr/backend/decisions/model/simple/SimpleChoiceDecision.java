package pl.decisr.backend.decisions.model.simple;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionType;

@Document
@RequiredArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SimpleChoiceDecision extends Decision {

    private final String title;
    private final String firstImagePath;
    private final String secondImagePath;

    @Override
    public DecisionType getType() {
        return DecisionType.SIMPLE_CHOICE;
    }
}