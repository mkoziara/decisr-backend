package pl.decisr.backend.decisions.model.simple;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.DecisionType;

@RequiredArgsConstructor
@Getter
public class SimpleChoiceAnswer extends DecisionAnswer {

    private final SimpleChoiceAnswerType answer;

    @Override
    public DecisionType getDecisionAnswerType() {
        return DecisionType.SIMPLE_CHOICE;
    }
}
