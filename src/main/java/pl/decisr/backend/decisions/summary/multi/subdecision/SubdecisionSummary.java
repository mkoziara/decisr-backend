package pl.decisr.backend.decisions.summary.multi.subdecision;


import lombok.Value;

@Value
public class SubdecisionSummary {
    private final String subDecisionId;
    private final Double score;
}
