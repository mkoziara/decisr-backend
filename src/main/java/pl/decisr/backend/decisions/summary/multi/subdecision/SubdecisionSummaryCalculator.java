package pl.decisr.backend.decisions.summary.multi.subdecision;


import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionAnswer;
import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionItemAnswer;
import pl.decisr.backend.decisions.model.multi.items.decision.MultiDecisionItem;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

@Component
public class SubdecisionSummaryCalculator {

    public SubdecisionSummary calculateSummary(List<MultiSubdecisionAnswer> subdecisionAnswers,
                                               List<MultiDecisionItem> items,
                                               String subdecisionId) {
        List<MultiSubdecisionItemAnswer> itemsAnswers = mergeDecisionAnswers(subdecisionAnswers);
        Double score = getSubdecisionScore(itemsAnswers, items);
        return new SubdecisionSummary(subdecisionId, score);
    }

    private List<MultiSubdecisionItemAnswer> mergeDecisionAnswers(List<MultiSubdecisionAnswer> multiAnswers) {
        return multiAnswers.stream()
                .flatMap(a -> a.getItemAnswers().stream())
                .collect(Collectors.toList());

    }

    private Double getSubdecisionScore(List<MultiSubdecisionItemAnswer> subdecisionItemAnswers,
                                       List<MultiDecisionItem> items) {
        Double score = 0d;
        for (MultiDecisionItem item : items) {
            String id = item.getId();
            Double sum = subdecisionItemAnswers.stream()
                    .filter(a -> a.getItemId().equals(id))
                    .flatMapToDouble(a -> DoubleStream.of(a.getAnswer()))
                    .sum();
            Double result = sum * item.getFactor();
            score += result;
        }
        return score;
    }
}
