package pl.decisr.backend.decisions.summary.complex.items;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.complex.items.AnsweredItem;

import java.util.List;


@Component
@RequiredArgsConstructor
public class ItemSummaryFactory {

    private final List<ItemSummaryCalculator> calculators;

    public ItemSummary createItemSummary(AnsweredItem answers) {
        return calculators.stream()
                .filter(s -> s.canCalculate(answers))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Unable to find calculator to create item summary"))
                .calculateSummary(answers);
    }
}
