package pl.decisr.backend.decisions.summary.complex.items.eitheror;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.complex.items.AnsweredItem;
import pl.decisr.backend.decisions.model.complex.items.eitheror.EitherOrItem;
import pl.decisr.backend.decisions.model.complex.items.eitheror.EitherOrItemAnswer;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummary;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummaryCalculator;

import java.util.List;

import static pl.decisr.backend.decisions.model.complex.items.DecisionItemType.EITHER_OR;

@Component
@RequiredArgsConstructor
public class EitherOrSummaryCalculator implements ItemSummaryCalculator<EitherOrItemAnswer> {

    @Override
    public boolean canCalculate(AnsweredItem answeredItem) {
        return answeredItem.getItemType().equals(EITHER_OR);
    }

    @Override
    public ItemSummary calculateSummary(AnsweredItem answeredItem) {
        EitherOrItem item = (EitherOrItem) answeredItem.getItem();
        String leftValue = item.getLeftValue();
        String rightValue = item.getRightValue();

        List<EitherOrItemAnswer> answers = mapAnswers(answeredItem.getAnswers());
        Long leftAnswers = countAnswers(answers, leftValue);
        Long rightAnswers = countAnswers(answers, rightValue);

        return new EitherOrItemSummary(item.getId(), leftValue, leftAnswers, rightValue, rightAnswers);
    }

    private Long countAnswers(List<EitherOrItemAnswer> answers, String answerValue) {
        return answers.stream()
                .filter(a -> a.getAnswer().equals(answerValue))
                .count();
    }
}
