package pl.decisr.backend.decisions.summary.multi;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.multi.MultiDecision;
import pl.decisr.backend.decisions.model.multi.answer.MultiAnswer;
import pl.decisr.backend.decisions.model.multi.answer.MultiSubdecisionAnswer;
import pl.decisr.backend.decisions.summary.SummaryCalculator;
import pl.decisr.backend.decisions.summary.multi.subdecision.SubdecisionSummary;
import pl.decisr.backend.decisions.summary.multi.subdecision.SubdecisionSummaryCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class MultiAnswerSummaryCalculator implements SummaryCalculator {

    private final SubdecisionSummaryCalculator subdecisionSummaryCalculator;

    @Override
    public boolean canCalculate(Decision decision) {
        return decision instanceof MultiDecision;
    }

    @Override
    public AnswerSummary calculateSummary(Decision decision) {
        MultiDecision multiDecision = (MultiDecision) decision;
        List<SubdecisionSummary> summaries = new ArrayList<>();

        List<MultiSubdecisionAnswer> answers = getSubdecisionsAnswers(decision.getAnswers());
        Map<String, List<MultiSubdecisionAnswer>> groupedByIds = groupAnswersBySubdecisionId(answers);

        for (Map.Entry<String, List<MultiSubdecisionAnswer>> itemAnswers : groupedByIds.entrySet()) {
            String subdecisionId = itemAnswers.getKey();
            List<MultiSubdecisionAnswer> subdecisionAnswers = itemAnswers.getValue();
            SubdecisionSummary subdecisionSummary = subdecisionSummaryCalculator
                    .calculateSummary(
                            subdecisionAnswers,
                            multiDecision.getItems(),
                            subdecisionId);
            summaries.add(subdecisionSummary);
        }
        return new MultiAnswerSummary(summaries);
    }

    private List<MultiSubdecisionAnswer> getSubdecisionsAnswers(List<DecisionAnswer> answers) {
        List<MultiAnswer> multiAnswers = mapAnswersToMulti(answers);
        return multiAnswers.stream()
                .flatMap(a -> a.getAnswers().stream())
                .collect(Collectors.toList());
    }

    private List<MultiAnswer> mapAnswersToMulti(List<DecisionAnswer> answers) {
        return answers.stream()
                .map(a -> (MultiAnswer) a)
                .collect(Collectors.toList());
    }

    private Map<String, List<MultiSubdecisionAnswer>> groupAnswersBySubdecisionId(List<MultiSubdecisionAnswer> subdecisionsAnswers) {
        return subdecisionsAnswers.stream()
                .collect(Collectors.groupingBy(MultiSubdecisionAnswer::getDecisionId));
    }
}
