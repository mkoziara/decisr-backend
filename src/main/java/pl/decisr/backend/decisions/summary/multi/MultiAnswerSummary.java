package pl.decisr.backend.decisions.summary.multi;

import lombok.Value;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.summary.multi.subdecision.SubdecisionSummary;

import java.util.List;

@Value
class MultiAnswerSummary implements AnswerSummary {

    private final String bestScoreId;
    private final List<SubdecisionSummary> summaries;

    public MultiAnswerSummary(List<SubdecisionSummary> summaries) {
        this.summaries = summaries;
        this.bestScoreId = getBestScoreId(summaries);
    }

    private String getBestScoreId(List<SubdecisionSummary> summaries) {
        String id = "";
        Double bestScore = 0d;
        for(SubdecisionSummary summary : summaries) {
            if(summary.getScore() > bestScore) {
                bestScore = summary.getScore();
                id = summary.getSubDecisionId();
            }
        }
        return id;
    }
}
