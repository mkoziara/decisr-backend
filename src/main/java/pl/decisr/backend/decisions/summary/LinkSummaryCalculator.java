package pl.decisr.backend.decisions.summary;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.link.LinkDecision;

@Component
@RequiredArgsConstructor
public class LinkSummaryCalculator implements SummaryCalculator {

    private final SimpleChoiceSummaryCalculator simpleChoiceSummaryCalculator;

    @Override
    public boolean canCalculate(Decision decision) {
        return decision instanceof LinkDecision;
    }

    @Override
    public AnswerSummary calculateSummary(Decision decision) {
        LinkDecision linkDecision = (LinkDecision) decision;

        return simpleChoiceSummaryCalculator.calculateSummary(linkDecision.getContent());
    }
}
