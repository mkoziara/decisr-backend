package pl.decisr.backend.decisions.summary;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.model.Decision;

import java.util.List;

@Component
@RequiredArgsConstructor
public class AnswerSummaryFactory {

    private final List<SummaryCalculator> summaryCalculators;

    public AnswerSummary createAnswerSummary(Decision decision) {
        return summaryCalculators.stream()
                .filter(s -> s.canCalculate(decision))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Unable to find calculator to create answer summary"))
                .calculateSummary(decision);
    }
}
