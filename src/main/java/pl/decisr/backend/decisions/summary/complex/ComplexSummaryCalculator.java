package pl.decisr.backend.decisions.summary.complex;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.complex.ComplexDecision;
import pl.decisr.backend.decisions.model.complex.answer.ComplexAnswer;
import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;
import pl.decisr.backend.decisions.model.complex.items.AnsweredItem;
import pl.decisr.backend.decisions.model.complex.items.DecisionItem;
import pl.decisr.backend.decisions.summary.SummaryCalculator;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummary;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummaryFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ComplexSummaryCalculator implements SummaryCalculator {

    private final ItemSummaryFactory itemSummaryFactory;

    @Override
    public boolean canCalculate(Decision decision) {
        return decision instanceof ComplexDecision;
    }

    @Override
    public AnswerSummary calculateSummary(Decision decision) {
        ComplexDecision complexDecision = (ComplexDecision) decision;

        List<DecisionAnswer> decisionAnswers = complexDecision.getAnswers();
        List<ComplexAnswer> complexAnswers = mapToComplexAnswers(decisionAnswers);

        List<ItemSummary> summaries = new ArrayList<>();
        Map<String, List<ItemAnswer>> groupedByIds = groupAnswersByIds(complexAnswers);

        for (Map.Entry<String, List<ItemAnswer>> itemAnswers : groupedByIds.entrySet()) {
            String itemId = itemAnswers.getKey();
            List<ItemAnswer> answers = itemAnswers.getValue();
            DecisionItem decisionItem = complexDecision.findItemById(itemId);

            AnsweredItem itemWithAnswers = new AnsweredItem(decisionItem, answers);
            ItemSummary itemSummary = itemSummaryFactory.createItemSummary(itemWithAnswers);
            summaries.add(itemSummary);
        }

        return new ComplexAnswerSummary(summaries);
    }

    private List<ComplexAnswer> mapToComplexAnswers(List<DecisionAnswer> decisionAnswers) {
        return decisionAnswers.stream()
                .map(a -> (ComplexAnswer) a)
                .collect(Collectors.toList());
    }

    private Map<String, List<ItemAnswer>> groupAnswersByIds(List<ComplexAnswer> complexAnswers) {
        List<ItemAnswer> allAnswers = mergeDecisionAnswers(complexAnswers);
        return allAnswers.stream()
                .collect(Collectors.groupingBy(ItemAnswer::getItemId));
    }

    private List<ItemAnswer> mergeDecisionAnswers(List<ComplexAnswer> complexAnswers) {
        return complexAnswers.stream()
                .flatMap(a -> a.getItemsAnswers().stream())
                .collect(Collectors.toList());
    }
}
