package pl.decisr.backend.decisions.summary.complex.items.slider;

import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.complex.items.AnsweredItem;
import pl.decisr.backend.decisions.model.complex.items.slider.SliderItem;
import pl.decisr.backend.decisions.model.complex.items.slider.SliderItemAnswer;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummary;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummaryCalculator;

import java.util.List;

import static pl.decisr.backend.decisions.model.complex.items.DecisionItemType.SLIDER;


@Component
public class SliderItemSummaryCalculator implements ItemSummaryCalculator<SliderItemAnswer> {
    @Override
    public boolean canCalculate(AnsweredItem answeredItem) {
        return answeredItem.getItemType().equals(SLIDER);
    }

    @Override
    public ItemSummary calculateSummary(AnsweredItem answeredItem) {
        SliderItem item = (SliderItem) answeredItem.getItem();

        List<SliderItemAnswer> answers = mapAnswers(answeredItem.getAnswers());
        Double average = countAverage(answers);

        return new SliderItemSummary(item.getId(), item.getDescription(), average);
    }

    private Double countAverage(List<SliderItemAnswer> answers) {
        return answers.stream()
                .mapToDouble(SliderItemAnswer::getAnswer)
                .average()
                .getAsDouble();
    }
}
