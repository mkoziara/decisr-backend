package pl.decisr.backend.decisions.summary.complex.items.eitheror;


import lombok.Value;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummary;

@Value
class EitherOrItemSummary implements ItemSummary {

    private final String itemId;
    private final String leftValue;
    private final Long leftAnswers;
    private final String rightValue;
    private final Long rightAnswers;
}
