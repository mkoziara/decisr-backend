package pl.decisr.backend.decisions.summary;

import org.springframework.stereotype.Component;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.simple.SimpleChoiceAnswer;
import pl.decisr.backend.decisions.model.simple.SimpleChoiceAnswerSummary;
import pl.decisr.backend.decisions.model.simple.SimpleChoiceAnswerType;
import pl.decisr.backend.decisions.model.simple.SimpleChoiceDecision;

import java.util.List;

@Component
public class SimpleChoiceSummaryCalculator implements SummaryCalculator {

    @Override
    public boolean canCalculate(Decision decision) {
        return decision instanceof SimpleChoiceDecision;
    }

    @Override
    public AnswerSummary calculateSummary(Decision decision) {
        Long leftAnswers = countAnswers(decision.getAnswers(), SimpleChoiceAnswerType.LEFT);
        Long rightAnswers = countAnswers(decision.getAnswers(), SimpleChoiceAnswerType.RIGHT);

        return new SimpleChoiceAnswerSummary(leftAnswers, rightAnswers);
    }

    private Long countAnswers(List<DecisionAnswer> answers, SimpleChoiceAnswerType answerType) {
        return answers.stream()
                .map(a -> (SimpleChoiceAnswer) a)
                .filter(a -> a.getAnswer().equals(answerType))
                .count();
    }
}
