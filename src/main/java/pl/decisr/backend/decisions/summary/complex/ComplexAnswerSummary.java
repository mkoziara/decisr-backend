package pl.decisr.backend.decisions.summary.complex;

import lombok.Value;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummary;

import java.util.List;

@Value
class ComplexAnswerSummary implements AnswerSummary {

    private final List<ItemSummary> summaries;
}
