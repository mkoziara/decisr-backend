package pl.decisr.backend.decisions.summary;

import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.model.Decision;

public interface SummaryCalculator {

    boolean canCalculate(Decision decision);

    AnswerSummary calculateSummary(Decision decision);

}
