package pl.decisr.backend.decisions.summary.complex.items.slider;


import lombok.Value;
import pl.decisr.backend.decisions.summary.complex.items.ItemSummary;

@Value
class SliderItemSummary implements ItemSummary {

    private final String itemId;
    private final String value;
    private final Double average;
}
