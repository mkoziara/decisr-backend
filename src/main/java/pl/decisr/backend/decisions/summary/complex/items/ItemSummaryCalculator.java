package pl.decisr.backend.decisions.summary.complex.items;


import pl.decisr.backend.decisions.model.complex.answer.ItemAnswer;
import pl.decisr.backend.decisions.model.complex.items.AnsweredItem;

import java.util.List;
import java.util.stream.Collectors;

public interface ItemSummaryCalculator<T extends ItemAnswer> {

    boolean canCalculate(AnsweredItem answeredItem);

    ItemSummary calculateSummary(AnsweredItem answeredItem);

    default List<T> mapAnswers(List<ItemAnswer> answers) {
        return answers.stream()
                .map(a -> (T) a)
                .collect(Collectors.toList());
    }
}
