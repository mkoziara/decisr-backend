package pl.decisr.backend.decisions;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.decisr.backend.decisions.database.DecisionsRepository;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionState;
import pl.decisr.backend.decisions.validators.DecisionNotNullValidator;
import pl.decisr.backend.decisions.validators.DecisionOwnerValidator;
import pl.decisr.backend.decisions.validators.NotFinishedDecisionValidator;

import java.security.Principal;

@Service
@RequiredArgsConstructor
public class DecisionFinishService {

    private final DecisionsRepository decisionsRepository;
    private final DecisionOwnerValidator decisionOwnerValidator;
    private final NotFinishedDecisionValidator notFinishedDecisionValidator;
    private final DecisionNotNullValidator decisionNotNullValidator;

    public Decision finishDecision(Decision decision, Principal principal) {
        decisionNotNullValidator.validateDecisionIsNotNull(decision);
        decisionOwnerValidator.validateDecisionIsOwnedByUser(decision, principal);
        notFinishedDecisionValidator.validateDecisionIsNotFinished(decision);

        decision.setDecisionState(DecisionState.FINISHED);
        return decisionsRepository.save(decision);
    }

}
