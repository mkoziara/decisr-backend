package pl.decisr.backend.decisions;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.decisr.backend.decisions.database.DecisionsRepository;
import pl.decisr.backend.decisions.model.AnswerSummary;
import pl.decisr.backend.decisions.model.Decision;
import pl.decisr.backend.decisions.model.DecisionAnswer;
import pl.decisr.backend.decisions.model.complex.answer.ComplexAnswerValidator;
import pl.decisr.backend.decisions.model.multi.validators.MultiAnswerValidator;
import pl.decisr.backend.decisions.model.multi.validators.MultiDecisionValidator;
import pl.decisr.backend.decisions.processors.DecisionPreprocessor;
import pl.decisr.backend.decisions.summary.AnswerSummaryFactory;
import pl.decisr.backend.decisions.validators.AlreadyAnsweredValidator;
import pl.decisr.backend.decisions.validators.NotFinishedDecisionValidator;
import pl.decisr.backend.decisions.validators.NotMyDecisionValidator;
import pl.decisr.backend.storage.FilePath;
import pl.decisr.backend.storage.FirebaseUploadService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class DecisionsController {

    private final DecisionFinishService decisionFinishService;

    private final NotFinishedDecisionValidator notFinishedDecisionValidator;
    private final AlreadyAnsweredValidator alreadyAnsweredValidator;
    private final NotMyDecisionValidator notMyDecisionValidator;
    private final DecisionsRepository decisionsRepository;
    private final AnswerSummaryFactory answerSummaryFactory;
    private final DecisionPreprocessor decisionPreprocessor;
    private final FirebaseUploadService firebaseUploadService;
    private final ComplexAnswerValidator complexAnswerValidator;
    private final LinkDecisionContentService linkDecisionContentService;
    private final MultiDecisionValidator multiDecisionValidator;
    private final MultiAnswerValidator multiAnswerValidator;

    @PostMapping("/decisions")
    public Decision createDecision(@Valid @RequestBody Decision decision, Principal principal) {
        if (multiDecisionValidator.canValidate(decision)) {
            multiDecisionValidator.validate(decision);
        }

        decision.setOwnerId(principal.getName());
        Decision processedDecision = decisionPreprocessor.processDecision(decision);
        processedDecision.setOwnerId(principal.getName());

        return decisionsRepository.save(processedDecision);
    }

    @GetMapping("/decisions")
    public List<Decision> getAllMyDecisions(Principal principal) {
        return decisionsRepository.findByOwnerId(principal.getName())
                .stream()
                .map(linkDecisionContentService::getDecisionContent)
                .map(this::fillWithSummary)
                .collect(Collectors.toList());
    }

    @PostMapping("/decisions/{id}/answer")
    public void postAnswer(@PathVariable("id") String id,
                           @RequestBody @Valid DecisionAnswer decisionAnswer,
                           Principal principal) {
        Decision decision = decisionsRepository.findById(id);
        notMyDecisionValidator.validateDecisionIsNotMine(decision, principal);
        alreadyAnsweredValidator.validateUserDidNotAlreadyAnswered(decision, principal);
        notFinishedDecisionValidator.validateDecisionIsNotFinished(decision);

        if (complexAnswerValidator.canValidate(decision, decisionAnswer)) {
            complexAnswerValidator.validate(decision, decisionAnswer);
        } else if (multiAnswerValidator.canValidate(decision, decisionAnswer)) {
            multiAnswerValidator.validate(decision, decisionAnswer);
        }

        decisionAnswer.setAnsweredBy(principal.getName());
        decisionsRepository.addAnswer(decision, decisionAnswer);
    }

    @GetMapping("otherDecisions")
    public List<Decision> getOtherDecisions(Principal principal) {
        return decisionsRepository.findUnansweredNotMineDecisions(principal.getName());
    }

    @PutMapping("/decisions/{id}/finish")
    public Decision finishDecision(@PathVariable("id") String id,
                                   Principal principal) {
        Decision decision = decisionsRepository.findById(id);
        return decisionFinishService.finishDecision(decision, principal);
    }

    @PostMapping("/upload")
    public FilePath upload(@RequestParam("file") MultipartFile file, Principal principal) {
        return firebaseUploadService.upload(file, principal);
    }

    private Decision fillWithSummary(Decision decision) {
        AnswerSummary summary = answerSummaryFactory.createAnswerSummary(decision);
        decision.setAnswerSummary(summary);

        return decision;
    }
}