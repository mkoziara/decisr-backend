package pl.decisr.backend.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;

@Configuration
public class FirebaseConfig {

    @Value("${firebase.config.file}")
    private String firebaseConfigFile;

    @Value("${firebase.config.project.id}")
    private String projectId;

    @Bean
    FirebaseApp firebaseAuth() throws IOException {
        FileInputStream fileInputStream = new FileInputStream(firebaseConfigFile);
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredential(FirebaseCredentials.fromCertificate(fileInputStream))
                .build();

        return FirebaseApp.initializeApp(options);
    }

    @Bean
    Storage storage() throws IOException {
        FileInputStream serviceAccount = new FileInputStream(firebaseConfigFile);
        GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
        return StorageOptions.newBuilder()
                .setCredentials(credentials)
                .setProjectId(projectId)
                .build().getService();
    }
}
