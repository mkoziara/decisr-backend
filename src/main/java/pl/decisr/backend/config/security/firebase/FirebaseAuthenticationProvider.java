package pl.decisr.backend.config.security.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.tasks.Task;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import pl.decisr.backend.config.security.firebase.token.AuthenticatedUser;
import pl.decisr.backend.config.security.firebase.token.FirebaseAuthenticationToken;
import pl.decisr.backend.config.security.firebase.token.InvalidTokenException;

import java.util.Collections;

@Component
@Profile("!dev")
public class FirebaseAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Override
    public boolean supports(Class<?> authentication) {
        return FirebaseAuthenticationToken.class.isAssignableFrom(authentication);
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        FirebaseAuthenticationToken authenticationToken = (FirebaseAuthenticationToken) authentication;
        String token = authenticationToken.getToken();

        Task<FirebaseToken> validationTask = FirebaseAuth.getInstance().verifyIdToken(token);

        while(true) {
            if (validationTask.isComplete()) break;
        }

        if(validationTask.isSuccessful()) {
            FirebaseToken result = validationTask.getResult();
            return new AuthenticatedUser(result.getEmail(), result.getUid(), result.getName(), token, Collections.emptyList());
        } else {
            throw new InvalidTokenException(String.format("Token %s is invalid", token));
        }
    }
}