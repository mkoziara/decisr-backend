package pl.decisr.backend.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.decisr.backend.config.security.firebase.FirebaseAuthenticationEntryPoint;
import pl.decisr.backend.config.security.firebase.FirebaseAuthenticationFilter;
import pl.decisr.backend.config.security.firebase.FirebaseAuthenticationProvider;
import pl.decisr.backend.config.security.firebase.FirebaseAuthenticationSuccessHandler;

import java.util.Collections;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final FirebaseAuthenticationEntryPoint authenticationEntryPoint;
    private final FirebaseAuthenticationProvider firebaseAuthenticationProvider;
    private final FirebaseAuthenticationSuccessHandler firebaseAuthenticationSuccessHandler;

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return new ProviderManager(Collections.singletonList(firebaseAuthenticationProvider));
    }

    public FirebaseAuthenticationFilter firebaseAuthenticationFilter() throws Exception {
        FirebaseAuthenticationFilter firebaseAuthenticationFilter = new FirebaseAuthenticationFilter();
        firebaseAuthenticationFilter.setAuthenticationManager(authenticationManager());
        firebaseAuthenticationFilter.setAuthenticationSuccessHandler(firebaseAuthenticationSuccessHandler);

        return firebaseAuthenticationFilter;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(
                        "/swagger-ui.html",
                        "/swagger-ui.html/",
                        "/webjars/springfox-swagger-ui/**",
                        "/v2/api-docs",
                        "/swagger-resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http
                .addFilterBefore(firebaseAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

        http
                .headers().cacheControl();
    }
}
