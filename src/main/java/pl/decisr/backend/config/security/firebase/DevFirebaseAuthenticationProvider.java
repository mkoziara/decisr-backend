package pl.decisr.backend.config.security.firebase;

import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import pl.decisr.backend.config.security.firebase.token.AuthenticatedUser;
import pl.decisr.backend.config.security.firebase.token.FirebaseAuthenticationToken;

import java.util.Collections;

@Component
@Profile("dev")
public class DevFirebaseAuthenticationProvider extends FirebaseAuthenticationProvider {

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        FirebaseAuthenticationToken authenticationToken = (FirebaseAuthenticationToken) authentication;
        String token = authenticationToken.getToken();

        return new AuthenticatedUser("test@test.pl", token, "user", token, Collections.emptyList());
    }
}
