package pl.decisr.backend.config.security.firebase.token;

import org.springframework.security.core.AuthenticationException;

public class InvalidTokenException extends AuthenticationException {

    public InvalidTokenException(String explanation) {
        super(explanation);
    }
}
