package pl.decisr.backend.storage;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor
public class FilePath {
    private final String path;
}
