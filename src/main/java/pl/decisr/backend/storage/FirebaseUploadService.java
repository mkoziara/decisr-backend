package pl.decisr.backend.storage;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.Principal;

@Service
@RequiredArgsConstructor
public class FirebaseUploadService {

    private final FirebaseStorage firebaseStorage;

    public FilePath upload(MultipartFile file, Principal principal) {
        try {
            String path = firebaseStorage.store(file.getBytes(), principal.getName());
            return new FilePath(path);
        } catch (IOException e) {
            throw new UncheckedIOException("Couldn't get bytes of given file", e);
        }
    }
}
