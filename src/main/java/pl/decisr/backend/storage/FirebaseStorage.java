package pl.decisr.backend.storage;


import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;
import java.net.URLConnection;

@Component
@RequiredArgsConstructor
public class FirebaseStorage {

    private final Storage storage;
    private final Tika tika = new Tika();

    @Value("${firebase.config.bucket.name}")
    private String bucketName;

    public String storeFileFromUrl(String fileUrl, String ownerId) {
        try {
            URL url = new URL(fileUrl);
            URLConnection urlConnection = url.openConnection();
            InputStream inputStream = urlConnection.getInputStream();
            byte[] file = IOUtils.toByteArray(inputStream);
            return store(file, ownerId);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String store(byte[] file, String ownerId) {
        String contentType = getFileContentType(file);
        BlobContent blobContent = new BlobContent(file, contentType, ownerId);
        BlobInfo blobInfo = buildBlobInfo(blobContent);
        return storage
                .create(
                        blobInfo,
                        file,
                        Storage.BlobTargetOption.doesNotExist())
                .getName();
    }

    private String getFileContentType(byte[] file) {
        return tika.detect(file);
    }

    private BlobInfo buildBlobInfo(BlobContent blobContent) {
        String blobPath = buildBlobPath(blobContent);
        return BlobInfo
                .newBuilder(bucketName, blobPath)
                .setContentType(blobContent.getContentType())
                .build();
    }

    private String buildBlobPath(BlobContent blobContent) {
        String mediaPath = MediaPath.fromContentType(blobContent.getContentType()).getPath();
        String filename = generateFileNameWithExtension("jpg");
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .path(blobContent.getOwnerId())
                .pathSegment(mediaPath)
                .pathSegment(filename)
                .build();
        return uriComponents.toString();
    }

    private String generateFileNameWithExtension(String extension) {
        ObjectId objectID = new ObjectId();
        String filename = objectID.toString();
        return filename + "." + extension;
    }
}
