package pl.decisr.backend.storage;

import java.util.Arrays;

public enum MediaPath {
    IMAGE_TYPE ("image", "images"),
    VIDEO("video", "videos"),
    OTHER("", "other");


    private String contentType;
    private String path;

    MediaPath(String contentType, String path) {
        this.contentType = contentType;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    static MediaPath fromContentType(String contentType) {
        return Arrays.stream(values())
                .filter(c -> contentType.startsWith(c.contentType))
                .findFirst()
                .orElse(OTHER);
    }
}
