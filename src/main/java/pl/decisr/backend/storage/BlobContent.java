package pl.decisr.backend.storage;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
class BlobContent {
    private final byte[] bytes;
    private final String contentType;
    private final String ownerId;
}
